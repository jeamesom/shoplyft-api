# shoplyft-api

Setup:

1. Clone
2. `cd shoplyft-api && bin/setup`
3. `bin/shoplyft-api`

Test:

1. From another terminal, run `curl -i --raw http://localhost:3000/`
