import Epoch
import HTTP
import Router

let router = Router { router in
  router.get("/") { request in
    print("REQUEST", request, separator: "\n")
    return Response(status: .OK, body: "hello, shoplyft\n")
  }
}

let port = 3000
let server = Server(port: port, responder: router)

print("Starting shoplyft-api on port \(port)...")
print("")

server.start()
