import PackageDescription

let package = Package(
  targets: [
    Target(name: "shoplyft-api"),
  ],
  dependencies: [
    .Package(url: "https://github.com/Zewo/Epoch.git", majorVersion: 0),
    .Package(url: "https://github.com/Zewo/Router.git", majorVersion: 0),
  ])
